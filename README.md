# DBLog Retention Policy

## Contents of this file

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Maintainers

## Introduction

DBLog Retention Policy provides the ability to control retention of log messages at a much more granular level
than core. You can set any logging channel (type) or group of channels in the dblog to keep a specified number
of log messages or keep by date.

For example keep node change records for 6 months and other logs for 1 month.

A default policy can and should be configured for all non-specified types.

## Requirements

This module requires Drupal core >= 9.0 with the core dblog module installed.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

* Enable the module through UI or Drush.
* Navigate to /admin/config/development/logging/retention_policy to configure retention policies.
* Create a "Default" policy by selecting `Default` in the list of Log Types - this applies to *all* log types as a
  group that don't have a specific policy.
* Create as many specific policies as required to meet your logging preferences/needs.

Policies that have apply to multiple log types apply to them in aggregate - so if keeping a certain number of log
messages that may not be the desired effect. Also, when limiting log messages by both date and time, it is the greater
of those values that is effectively used.

## Maintainers

* [Nick Dickinson-Wilde](https://drupal.org/u/nickdickinsonwilde)
