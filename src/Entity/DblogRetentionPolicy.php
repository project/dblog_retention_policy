<?php

declare(strict_types = 1);

namespace Drupal\dblog_retention_policy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\dblog_retention_policy\DblogRetentionPolicyStorageInterface;

/**
 * Defines the DBLog Retention Policy entity type.
 *
 * @ConfigEntityType(
 *   id = "dblog_retention_policy",
 *   label = @Translation("DBLog Retention Policy"),
 *   label_collection = @Translation("DBLog Retention Policies"),
 *   label_singular = @Translation("DBLog Retention Policy"),
 *   label_plural = @Translation("DBLog Retention Polices"),
 *   label_count = @PluralTranslation(
 *     singular = "@count DBLog Retention Policy",
 *     plural = "@count DBLog Retention Policies",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\dblog_retention_policy\DblogRetentionPolicyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\dblog_retention_policy\Form\DblogRetentionPolicyForm",
 *       "edit" = "Drupal\dblog_retention_policy\Form\DblogRetentionPolicyForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "dblog_retention_policy",
 *   admin_permission = "administer DBLog Retention Policy",
 *   links = {
 *     "collection" = "/admin/config/development/logging/retention_policy",
 *     "add-form" = "/admin/config/development/logging/retention_policy/add",
 *     "edit-form" = "/admin/config/development/logging/retention_policy/{dblog_retention_policy}",
 *     "delete-form" = "/admin/config/development/logging/retention_policy/{dblog_retention_policy}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "types",
 *     "limit_count",
 *     "limit_time"
 *   }
 * )
 */
class DblogRetentionPolicy extends ConfigEntityBase implements DblogRetentionPolicyStorageInterface {

  /**
   * The DBLog Retention Policy ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The DBLog Retention Policy label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The DBLog Retention Policy description.
   *
   * @var string
   */
  protected ?string $description;

  /**
   * The log types this policy applies to.
   *
   * @var array
   */
  protected array $types;

  /**
   * The limit to use when clearing by number of records.
   *
   * @var int|null
   */
  protected ?int $limit_count;

  /**
   * The time to keep when clearing by number of records.
   *
   * @var int|null
   */
  protected ?int $limit_time;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, string $entity_type) {
    parent::__construct($values, $entity_type);
    $this->database = \Drupal::database();
  }

  /**
   * Get the applicable log types.
   *
   * @return string[]
   *   The log types.
   */
  public function getTypes(): array {
    return $this->types;
  }

  /**
   * Clear the log as per current policy configuration.
   */
  public function clear(): void {
    $min_row = $this->database->select('watchdog', 'w')
      ->fields('w', ['wid'])
      ->orderBy('wid', 'DESC')
      ->condition('type', $this->types, 'IN');
    if ($this->limit_count) {
      $min_row->range($this->limit_count - 1, 1);
    }
    if ($this->limit_time) {
      $min_row->condition('timestamp', time() - $this->limit_time, '<');
    }

    // Delete all log messages if any to be deleted.
    if ($min_row = $min_row->execute()->fetchField()) {
      $this->database->delete('watchdog')
        ->condition('wid', $min_row, '<')
        ->condition('type', $this->types, 'IN')
        ->execute();
    }
  }

  /**
   * Clear for default option.
   *
   * Uses policy configuration for limits but types use NOT IN comparator
   * instead.
   *
   * @param string[] $types
   *   The otherwise configured types.
   */
  public function clearDefault(array $types): void {
    $min_row = $this->database->select('watchdog', 'w')
      ->fields('w', ['wid'])
      ->orderBy('wid', 'DESC')
      ->condition('type', $types, 'NOT IN');
    if ($this->limit_count) {
      $min_row->range($this->limit_count - 1, 1);
    }
    if ($this->limit_time) {
      $min_row->condition('timestamp', time() - $this->limit_time, '<');
    }

    // Delete all log messages if any to be deleted.
    if ($min_row = $min_row->execute()->fetchField()) {
      $this->database->delete('watchdog')
        ->condition('wid', $min_row, '<')
        ->condition('type', $types, 'NOT IN')
        ->execute();
    }
  }

}
