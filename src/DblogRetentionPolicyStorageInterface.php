<?php

declare(strict_types = 1);

namespace Drupal\dblog_retention_policy;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Dblog Retention Policy entity type.
 */
interface DblogRetentionPolicyStorageInterface extends ConfigEntityInterface {

}
