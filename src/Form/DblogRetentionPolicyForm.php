<?php

declare(strict_types = 1);

namespace Drupal\dblog_retention_policy\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DBLog Retention Policy form.
 *
 * @property \Drupal\dblog_retention_policy\DblogRetentionPolicyStorageInterface $entity
 */
class DblogRetentionPolicyForm extends EntityForm {

  /**
   * Constructs a new form instance.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the DBLog Retention Policy.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\dblog_retention_policy\Entity\DblogRetentionPolicy::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $types = $this->state->get('dblog_retention_policy.types', []);
    foreach (_dblog_get_message_types() as $type) {
      $types[$type] = $type;
    }
    $this->state->set('dblog_retention_policy.types', $types);
    $types = ['default' => $this->t('Default')] + $types;
    $selected = $this->entity->get('types') ?? [];
    foreach ($selected as $type) {
      $types[$type] = $type;
    }
    $form['types'] = [
      '#title' => $this->t('Log Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $types,
      '#default_value' => $selected,
    ];
    $form['limit_help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Select one or both methods to limit retained log messages. If both are selected, the larger value wins.
        Example: limit of 1000 records and 1 month, if there are 2k records in the month it will retain all of those, or if there are at only 500 records per month
        it will retain 1000 even if that means it is 2 months worth.'),
    ];

    $row_limits = [100, 1000, 10000, 100000, 1000000];
    $form['limit_count'] = [
      '#type' => 'select',
      '#title' => $this->t('Count limit'),
      '#default_value' => $this->entity->get('limit_count') ?? 0,
      '#options' => [0 => t('All')] + array_combine($row_limits, $row_limits),
      '#description' => $this->t('Number of messages to keep in the database log.'),
    ];

    $form['limit_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Time Limit'),
      '#default_value' => $this->entity->get('limit_time') ?? 0,
      '#description' => $this->t('Time limit to keep messages in the database log'),
      '#options' => [
        0 => $this->t('All'),
        86400 => $this->t('1 day'),
        604800 => $this->t('1 week'),
        1298600 => $this->t('2 weeks'),
        2592000 => $this->t('1 month'),
        7776000 => $this->t('3 months'),
        15552000 => $this->t('6 months'),
        31536000 => $this->t('1 year'),
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Optional Description of the DBLog Retention Policy.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result === SAVED_NEW
      ? $this->t('Created new Dblog Retention Policy %label.', $message_args)
      : $this->t('Updated Dblog Retention Policy %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

}
